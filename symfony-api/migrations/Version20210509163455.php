<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210509163455 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE test_id_seq CASCADE');
        $this->addSql('CREATE TABLE indicateur (id INT NOT NULL, dep INT DEFAULT NULL, date DATE DEFAULT NULL, reg INT DEFAULT NULL, tx_pos DOUBLE PRECISION DEFAULT NULL, tx_incid DOUBLE PRECISION DEFAULT NULL, to1 DOUBLE PRECISION DEFAULT NULL, r DOUBLE PRECISION DEFAULT NULL, hosp INT DEFAULT NULL, rea INT DEFAULT NULL, rad INT DEFAULT NULL, dchosp INT DEFAULT NULL, rea_reg INT DEFAULT NULL, incid_hosp INT DEFAULT NULL, incid_rea INT DEFAULT NULL, incid_rad INT DEFAULT NULL, incid_dchosp INT DEFAULT NULL, reg_incid_rea INT DEFAULT NULL, pos INT DEFAULT NULL, pos_7j INT DEFAULT NULL, cv_dose1 DOUBLE PRECISION DEFAULT NULL, lib_reg VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE test_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('DROP TABLE indicateur');
    }
}
