<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\IndicateurRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=IndicateurRepository::class)
 */
class Indicateur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dep;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reg;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tx_pos;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tx_incid;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $TO1;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $R;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hosp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rea;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rad;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dchosp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rea_reg;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $incid_hosp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $incid_rea;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $incid_rad;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $incid_dchosp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reg_incid_rea;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pos;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pos_7j;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $cv_dose1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lib_reg;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDep(): ?string
    {
        return $this->dep;
    }

    public function setDep(?string $dep): self
    {
        $this->dep = $dep;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getReg(): ?int
    {
        return $this->reg;
    }

    public function setReg(?int $reg): self
    {
        $this->reg = $reg;

        return $this;
    }

    public function getTxPos(): ?float
    {
        return $this->tx_pos;
    }

    public function setTxPos(?float $tx_pos): self
    {
        $this->tx_pos = $tx_pos;

        return $this;
    }

    public function getTxIncid(): ?float
    {
        return $this->tx_incid;
    }

    public function setTxIncid(?float $tx_incid): self
    {
        $this->tx_incid = $tx_incid;

        return $this;
    }

    public function getTO1(): ?float
    {
        return $this->TO1;
    }

    public function setTO1(?float $TO1): self
    {
        $this->TO1 = $TO1;

        return $this;
    }

    public function getR(): ?float
    {
        return $this->R;
    }

    public function setR(?float $R): self
    {
        $this->R = $R;

        return $this;
    }

    public function getHosp(): ?int
    {
        return $this->hosp;
    }

    public function setHosp(?int $hosp): self
    {
        $this->hosp = $hosp;

        return $this;
    }

    public function getRea(): ?int
    {
        return $this->rea;
    }

    public function setRea(?int $rea): self
    {
        $this->rea = $rea;

        return $this;
    }

    public function getRad(): ?int
    {
        return $this->rad;
    }

    public function setRad(?int $rad): self
    {
        $this->rad = $rad;

        return $this;
    }

    public function getDchosp(): ?int
    {
        return $this->dchosp;
    }

    public function setDchosp(?int $dchosp): self
    {
        $this->dchosp = $dchosp;

        return $this;
    }

    public function getReaReg(): ?int
    {
        return $this->rea_reg;
    }

    public function setReaReg(?int $rea_reg): self
    {
        $this->rea_reg = $rea_reg;

        return $this;
    }

    public function getIncidHosp(): ?int
    {
        return $this->incid_hosp;
    }

    public function setIncidHosp(?int $incid_hosp): self
    {
        $this->incid_hosp = $incid_hosp;

        return $this;
    }

    public function getIncidRea(): ?int
    {
        return $this->incid_rea;
    }

    public function setIncidRea(?int $incid_rea): self
    {
        $this->incid_rea = $incid_rea;

        return $this;
    }

    public function getIncidRad(): ?int
    {
        return $this->incid_rad;
    }

    public function setIncidRad(?int $incid_rad): self
    {
        $this->incid_rad = $incid_rad;

        return $this;
    }

    public function getIncidDchosp(): ?int
    {
        return $this->incid_dchosp;
    }

    public function setIncidDchosp(?int $incid_dchosp): self
    {
        $this->incid_dchosp = $incid_dchosp;

        return $this;
    }

    public function getRegIncidRea(): ?int
    {
        return $this->reg_incid_rea;
    }

    public function setRegIncidRea(?int $reg_incid_rea): self
    {
        $this->reg_incid_rea = $reg_incid_rea;

        return $this;
    }

    public function getPos(): ?int
    {
        return $this->pos;
    }

    public function setPos(?int $pos): self
    {
        $this->pos = $pos;

        return $this;
    }

    public function getPos7j(): ?int
    {
        return $this->pos_7j;
    }

    public function setPos7j(?int $pos_7j): self
    {
        $this->pos_7j = $pos_7j;

        return $this;
    }

    public function getCvDose1(): ?float
    {
        return $this->cv_dose1;
    }

    public function setCvDose1(?float $cv_dose1): self
    {
        $this->cv_dose1 = $cv_dose1;

        return $this;
    }

    public function getLibReg(): ?string
    {
        return $this->lib_reg;
    }

    public function setLibReg(?string $lib_reg): self
    {
        $this->lib_reg = $lib_reg;

        return $this;
    }
    public function toArray()
    {
        return array(
            'id' => $this->getId(),
            'dep' => $this->getDep(),
            'date' => $this->getDep(),
            'reg' => $this->getReg(),
            'tx_pos' => $this->getTxPos(),
            'tx_incid' => $this->getTxIncid(),
            'TO1' => $this->getTO1(),
            'R' => $this->getR(),
            'hosp' => $this->getHosp(),
            'rea' => $this->getRea(),
            'rad' => $this->getRad(),
            'dchosp' => $this->getDchosp(),
            'rea_reg' => $this->getReaReg(),
            'incid_hosp' => $this->getIncidHosp(),
            'incid_rea' => $this->getIncidRea(),
            'incid_rad' => $this->getIncidRad(),
            'incid_dchosp' => $this->getIncidDchosp(),
            'reg_incid_rea' => $this->getRegIncidRea(),
            'pos' => $this->getPos(),
            'pos_7j' => $this->getPos7j(),
            'cv_dose1' => $this->getCvDose1(),
            'lib_reg' => $this->getLibReg()
        );
    }
}
