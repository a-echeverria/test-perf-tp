<?php

namespace App\Form;

use App\Entity\Indicateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IndicateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dep')
            ->add('date')
            ->add('reg')
            ->add('tx_pos')
            ->add('tx_incid')
            ->add('TO1')
            ->add('R')
            ->add('hosp')
            ->add('rea')
            ->add('rad')
            ->add('dchosp')
            ->add('rea_reg')
            ->add('incid_hosp')
            ->add('incid_rea')
            ->add('incid_rad')
            ->add('incid_dchosp')
            ->add('reg_incid_rea')
            ->add('pos')
            ->add('pos_7j')
            ->add('cv_dose1')
            ->add('lib_reg')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Indicateur::class,
        ]);
    }
}
