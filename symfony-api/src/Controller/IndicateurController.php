<?php

namespace App\Controller;

use ApiPlatform\Core\Serializer\JsonEncoder;
use App\Entity\Indicateur;
use App\Form\IndicateurType;
use App\Repository\IndicateurRepository;
use phpDocumentor\Reflection\Types\Object_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("api/indicateurs")
 */
class IndicateurController extends AbstractController
{
    /**
     * @Route("/last", name="indicateur_indexLast", methods={"GET"})
     */
    public function indexLast(IndicateurRepository $indicateurRepository): Response
    {

        $datas = $indicateurRepository->findLast(200);
        $arrayData = array();
        foreach ($datas as $data)
        {
            array_push($arrayData,$data->toArray());
        }
        return new Response(json_encode($arrayData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    /**
     * @Route("/big", name="indicateur_indexBig", methods={"GET"})
     */
    public function indexBig(IndicateurRepository $indicateurRepository): Response
    {

        $datas = $indicateurRepository->findLast(20000);
        $arrayData = array();
        foreach ($datas as $data)
        {
            array_push($arrayData,$data->toArray());
        }
        return new Response(json_encode($arrayData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    /**
     * @Route("/low", name="indicateur_low", methods={"GET"})
     */
    public function indexLow( ): Response
    {

        $arrayData = array();
        for($i = 0; $i <=30; $i++)
        {
            for($j = 0; $j <=30; $j++)
            {
                    for($k = 0; $k <=30; $k++)
                    {
                        $data = "i".$i;
                        $data .= ":j".$j;
                        $data .= ":k".$k;
                        $data .= ":key".rand(0,10000);
                        array_push($arrayData,$data);
                    }

            }
        }
        return new Response(json_encode($arrayData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }


}
