import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  stages: [
    { duration: '10s', target: 10 }, // below normal load
    { duration: '20s', target: 10 },
    { duration: '10s', target: 20 }, // normal load
    { duration: '20s', target: 20 },
    { duration: '10s', target: 30 }, // around the breaking point
    { duration: '20s', target: 30 },
    { duration: '10s', target: 40 }, // beyond the breaking point
    { duration: '20s', target: 40 },
    { duration: '40s', target: 0 }, // scale down. Recovery stage.
  ],
};

export default function () {
  const BASE_URL = 'http://nginx:80/api/indicateurs';

  let responses = http.batch([
    [
      'GET',
      `${BASE_URL}/1`,
      null,
      null,
    ],
    [
      'GET',
      `${BASE_URL}/low`,
      null,
      null,
    ],
    [
      'GET',
      `${BASE_URL}/last`,
      null,
      null,
    ],
    [
      'GET',
      `${BASE_URL}/big`,
      null,
      null,
    ],
  ]);

  sleep(1);
}
