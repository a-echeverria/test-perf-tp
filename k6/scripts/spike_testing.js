import http from 'k6/http';
import { sleep } from 'k6';

export let options = {
  stages: [
    { duration: '10s', target: 10 }, // below normal load
    { duration: '20s', target: 20 },
    { duration: '10s', target: 100 }, // spike to 100 users
    { duration: '60s', target: 100 }, // stay at 100 for 60s
    { duration: '10s', target: 20 }, // scale down. Recovery stage.
    { duration: '20s', target: 10 },
    { duration: '10s', target: 0 },
  ],
};
export default function () {
  const BASE_URL = 'http://nginx:80/api/indicateurs';

  let responses = http.batch([
    [
      'GET',
      `${BASE_URL}/1`,
      null,
      null,
    ],
    [
      'GET',
      `${BASE_URL}/low`,
      null,
      null,
    ],
    [
      'GET',
      `${BASE_URL}/last`,
      null,
      null,
    ],
    [
      'GET',
      `${BASE_URL}/big`,
      null,
      null,
    ],
  ]);

  sleep(1);
}
