import React, {useContext} from "react";
import { Link } from "react-router-dom";
import {IndicateurContext} from "../contexts";

export function Header() {
  const token = localStorage.getItem("token");
  const {logout} = useContext(IndicateurContext)

  return (
    <nav>
      {!token && (
        <Link to="/login">Se connecter</Link>
      )}
      {token && (
        <a onClick={logout}>Se déconnecter</a>
      )}
      <Link to="/low">Low</Link>
      <Link to="/big">Big</Link>
      <Link to="/last">Last</Link>
      <Link to="/new">New</Link>
    </nav>
  );
}
