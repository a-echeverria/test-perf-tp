import React, {useContext, useEffect, useState} from "react";
import {useParams} from "react-router";
import {IndicateurContext} from "../../contexts";

export function FindOne({}) {
  const {id} = useParams()
  const [list, setList] = useState([]);
  const {findOne, deleteOne} = useContext(IndicateurContext);

  useEffect(async () => setList(await findOne(id)), []);

  return (
    <div>
      id: {list.id}<br/>
      dep: {list.dep}<br/>
      date: {list.date}<br/>
      reg: {list.reg}<br/>
      tx_pos: {list.tx_pos}<br/>
      tx_incid: {list.tx_incid}<br/>
      TO1: {list.TO1}<br/>
      R: {list.R}<br/>
      hosp: {list.hosp}<br/>
      rea: {list.rea}<br/>
      rad: {list.rad}<br/>
      dchosp: {list.dchosp}<br/>
      rea_reg: {list.rea_reg}<br/>
      incid_hosp: {list.incid_hosp}<br/>
      incid_rea: {list.incid_rea}<br/>
      incid_rad: {list.incid_rad}<br/>
      incid_dchosp: {list.incid_dchosp}<br/>
      reg_incid_rea: {list.reg_incid_rea}<br/>
      pos: {list.pos}<br/>
      pos_7j: {list.pos_7j}<br/>
      cv_dose1: {list.cv_dose1}<br/>
      lib_reg: {list.lib_reg}<br/><br/>
      <a onClick={async () => {deleteOne(list.id)}}>Delete</a>
    </div>
  );
}
