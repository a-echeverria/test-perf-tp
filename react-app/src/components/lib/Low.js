import React, {useContext, useEffect, useState} from "react";
import {IndicateurContext} from "../../contexts";

export function Low({}) {
  const [list, setList] = useState([]);
  const {low} = useContext(IndicateurContext);

  useEffect(async () => setList(await low()), []);

  return (
    <div>
      {list && (
        <div>
          <ul>
            {list.map((elem) => (
              <li key={elem}>
                {elem}
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}
