import React, {useContext, useEffect, useState} from "react";
import {IndicateurContext} from "../../contexts";

export function Last({}) {
  const [list, setList] = useState([]);
  const {last} = useContext(IndicateurContext);

  useEffect(async () => setList(await last()), []);

  return (
    <div>
      {list && (
        <div>
          <ul>
            {list.map((elem) => (
              <li key={elem.id}>
                id: {elem.id}<br/>
                dep: {elem.dep}<br/>
                date: {elem.date}<br/>
                reg: {elem.reg}<br/>
                tx_pos: {elem.tx_pos}<br/>
                tx_incid: {elem.tx_incid}<br/>
                TO1: {elem.TO1}<br/>
                R: {elem.R}<br/>
                hosp: {elem.hosp}<br/>
                rea: {elem.rea}<br/>
                rad: {elem.rad}<br/>
                dchosp: {elem.dchosp}<br/>
                rea_reg: {elem.rea_reg}<br/>
                incid_hosp: {elem.incid_hosp}<br/>
                incid_rea: {elem.incid_rea}<br/>
                incid_rad: {elem.incid_rad}<br/>
                incid_dchosp: {elem.incid_dchosp}<br/>
                reg_incid_rea: {elem.reg_incid_rea}<br/>
                pos: {elem.pos}<br/>
                pos_7j: {elem.pos_7j}<br/>
                cv_dose1: {elem.cv_dose1}<br/>
                lib_reg: {elem.lib_reg}<br/>
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}
