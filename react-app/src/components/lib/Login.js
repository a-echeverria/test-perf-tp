import React, {useContext, useState} from "react";
import {IndicateurContext} from "../../contexts";

export function Login({}) {
  const {login} = useContext(IndicateurContext);
  const [user, setUser] = useState({
    username: '',
    password: '',
  });

  const handleSubmit = () => {
    login(user);
  };

  const handleChange = (event) => {
    setUser({
      ...user,
      [event.target.name]: event.target.value,
    });
  };

  return (
    <div>
      <label>Email:</label><br/>
      <input type={"text"} value={user.username} name={"username"} onChange={handleChange}/><br/>
      <label>Password:</label><br/>
      <input type={"password"} value={user.password} name={"password"} onChange={handleChange}/><br/>
      <button onClick={handleSubmit}>Se connecter</button>
    </div>
  );
}
