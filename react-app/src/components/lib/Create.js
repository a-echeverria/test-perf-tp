import React, {useContext, useState} from "react";
import {IndicateurContext} from "../../contexts";

export function Create({}) {
  const {createOne} = useContext(IndicateurContext);
  const [indicateur, setIndicateur] = useState({
    dep: "",
    date: new Date(),
    reg: 0,
    tx_pos: 0,
    tx_incid: 0,
    to1: 0,
    r: 0,
    hosp: 0,
    rea: 0,
    rad: 0,
    dchosp: 0,
    rea_reg: 0,
    incid_hosp: 0,
    incid_rea: 0,
    incid_rad: 0,
    incid_dchosp: 0,
    reg_incid_rea: 0,
    pos: 0,
    pos_7j: 0,
    cv_dose1: 0,
    lib_reg: "",
  });


  const handleChange = (event) => {
    setIndicateur({
      ...indicateur,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = () => {
    createOne(indicateur);
  }

  return (
    <div>
      <label>dep</label><br/>
      <input type={"text"} value={indicateur.dep} name={"dep"} onChange={handleChange}/><br/>
      <label>reg</label><br/>
      <input type={"number"} value={indicateur.reg} name={"reg"} onChange={handleChange}/><br/>
      <label>tx_pos</label><br/>
      <input type={"number"} value={indicateur.tx_pos} name={"tx_pos"} onChange={handleChange}/><br/>
      <label>tx_incid</label><br/>
      <input type={"number"} value={indicateur.tx_incid} name={"tx_incid"} onChange={handleChange}/><br/>
      <label>to1</label><br/>
      <input type={"number"} value={indicateur.to1} name={"to1"} onChange={handleChange}/><br/>
      <label>r</label><br/>
      <input type={"number"} value={indicateur.r} name={"r"} onChange={handleChange}/><br/>
      <label>hosp</label><br/>
      <input type={"number"} value={indicateur.hosp} name={"hosp"} onChange={handleChange}/><br/>
      <label>rea</label><br/>
      <input type={"number"} value={indicateur.rea} name={"rea"} onChange={handleChange}/><br/>
      <label>rad</label><br/>
      <input type={"number"} value={indicateur.rad} name={"rad"} onChange={handleChange}/><br/>
      <label>dchosp</label><br/>
      <input type={"number"} value={indicateur.dchosp} name={"dchosp"} onChange={handleChange}/><br/>
      <label>rea_reg</label><br/>
      <input type={"number"} value={indicateur.rea_reg} name={"rea_reg"} onChange={handleChange}/><br/>
      <label>incid_hosp</label><br/>
      <input type={"number"} value={indicateur.incid_hosp} name={"incid_hosp"} onChange={handleChange}/><br/>
      <label>incid_rea</label><br/>
      <input type={"number"} value={indicateur.incid_rea} name={"incid_rea"} onChange={handleChange}/><br/>
      <label>incid_rad</label><br/>
      <input type={"number"} value={indicateur.incid_rad} name={"incid_rad"} onChange={handleChange}/><br/>
      <label>incid_dchosp</label><br/>
      <input type={"number"} value={indicateur.incid_dchosp} name={"incid_dchosp"} onChange={handleChange}/><br/>
      <label>reg_incid_rea</label><br/>
      <input type={"number"} value={indicateur.reg_incid_rea} name={"reg_incid_rea"} onChange={handleChange}/><br/>
      <label>pos</label><br/>
      <input type={"number"} value={indicateur.pos} name={"pos"} onChange={handleChange}/><br/>
      <label>pos_7j</label><br/>
      <input type={"number"} value={indicateur.pos_7j} name={"pos_7j"} onChange={handleChange}/><br/>
      <label>cv_dose1</label><br/>
      <input type={"number"} value={indicateur.cv_dose1} name={"cv_dose1"} onChange={handleChange}/><br/>
      <label>lib_reg</label><br/>
      <input type={"text"} value={indicateur.lib_reg} name={"lib_reg"} onChange={handleChange}/><br/><br/>
      <button onClick={handleSubmit}>Générer un indicateur</button>
    </div>
  );
}
