import './App.css';
import {BrowserRouter, Route} from 'react-router-dom';
import {Big, Header, Last, Low, FindOne, Login, Create} from "./components"
import {IndicateurProvider} from "./contexts";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <IndicateurProvider>
          <BrowserRouter>
            <Route path="/" exact>
              <Header/>
            </Route>
            <Route path="/low" exact>
              <Header/>
              <Low/>
            </Route>
            <Route path="/big" exact>
              <Header/>
              <Big/>
            </Route>
            <Route path="/last" exact>
              <Header/>
              <Last/>
            </Route>
            <Route path="/indicateurs/:id" exact>
              <Header/>
              <FindOne/>
            </Route>
            <Route path="/login" exact>
              <Header/>
              <Login/>
            </Route>
            <Route path="/new" exact>
              <Header/>
              <Create/>
            </Route>
          </BrowserRouter>
        </IndicateurProvider>
      </header>
    </div>
  );
}

export default App;
